'use strict';

const { FOOBAR, MAX_COUNT } = process.env;

module.exports.handler = (event, context, callback) => {

  callback(null, {
    statusCode: 200,
    body: JSON.stringify({
      message: `Hello, the current time is ${new Date().toTimeString()}`,
      foobar: FOOBAR,
      maxCount: MAX_COUNT,
    }),
  });

};
