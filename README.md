# Serverless Boilerplate
[![serverless](http://public.serverless.com/badges/v3.svg)](http://www.serverless.com)

## Getting Started

With Serverless Framework v1.5 and later, a new project based on the project template is initialized with the command:

```
$ sls install -u https://bitbucket.org/tomdevops/serverless-basic-boilerplate -n myservicename
$ cd myservicename
$ npm install
```

Where `myservicename` refers to the ... wait for it ... the name of the service you're building.

## BitBucket?
You think you're code is good enough? Create a new repository in BitBucket and execute the following steps:

```
$ git init
$ git remote add origin <remote_repo_url>
$ git add .
$ git commit -m 'Initial commit'
$ git push
```

Where:

* `<remote_repo_url>` : For example: `git@bitbucket.org:tomdevops/mkb-serverless-docs.git`

## AWS Profiles

Our projects kinda assume you configured your profiles in `~/.aws/credentials` as follows:
> TL;DR: it's all about the naming convention.

```
;Admin account
[mkb]
aws_access_key_id = <YOUR ACCESS KEY>
aws_secret_access_key = <YOUR SECRET ACCESS KEY>

;Development account
[mkbdev]
role_arn = arn:aws:iam::<DEV_AWS_ACCOUNT_ID>:role/<ROLE>
source_profile = mkb
region = eu-west-1

;Test account
[mkbtest]
role_arn = arn:aws:iam::<TEST_AWS_ACCOUNT_ID>:role/<ROLE>
source_profile = mkb
region = eu-west-1

;UAT account
[mkbuat]
role_arn = arn:aws:iam::<UAT_AWS_ACCOUNT_ID>:role/<ROLE>
source_profile = mkb
region = eu-west-1
```

The main reason for this structure is the way these profile are referenced in `serverless` and `terraform` configuration files. Remember: you don't _have_ to update your `~/.aws/credentials`, but ... don't blame me if stuff breaks or doesn't work as intended ;)

## Deployment

Before you deploy your service, make sure you covered the following prerequisites:

For Terraform, execute the following steps:
```
$ cd infrastructure

$ cp terraform.tfvars.example terraform.tfvars
$ cp -R env/default env/<STAGE>
$ mv env/<STAGE>/terraform.tfvars.example env/<STAGE>/terraform.tfvars

# Before you continue, update `terraform.tfvars` and `env/<STAGE>/terraform.tfvars`!
```
> Pay special attention to setting `aws_route53_profile`: this should point to a AWS profile in `~/.aws/credentials` where Route53 is managed!

As a result, `terraform` should be configured properly. PS: this is a one-time only thing!

> The boilerplate is already CodeBuild compliant! Look at the `buildspec.yml` and `.gitmodules`. The CodeBuild project can be build using TF.
> 
> TODO: fix the inception! You first have to run TF manually, only then you can use CodeBuild.

Run the following steps to deploy your service successfully:

### Terraform

```
$ cd infrastructure
$ make <STAGE> plan
# -OR-
$ make <STAGE> apply
```
Where `<STAGE>` is one of: `['dev','test','uat','prod']`

### Serverless

```
$ sls deploy --aws-profile <PROFILE> --stage <STAGE> --verbose
```

As a result, a CloudFormation stack is deployed in the `<STAGE>` account.

## Technical Debt

This project is configured to use [`pre-commit`](https://github.com/observing/pre-commit) automatically. Whenever you do a `git commit`, the source code will be scanned for `TODO` references. Results will be written to `TODO.md` and pushed to the BitBucket repository.

### What's in it for me?
The `TODO.md` file contains all the things that we consider 'technical debt'. All items can be reviewed on a weekly basis by the team and translated to Jira tickets. Of course, after you fixed the issue, the related `TODO` should be removed from the code.

### Contribute
If you want to contribute to this `serverless` boilerplate, create a pull request.

~ the end
