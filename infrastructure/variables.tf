#
# The following variables are needed for a successful deployment.
# Variables for which you specified a value here, apply to all stages/environments.
# If a variable has a value, it can be overridden in the `env/<ENV>/terraform.tfvars` file.
#

variable "aws_region" {
  description = "Reference to the AWS region in which you deploy (can be overridden on `stage` level)"
  default     = "eu-west-1"
}

variable "aws_profile" {
  description = "Reference to AWS profile that is used for creating all AWS resources"
}

variable "aws_route53_profile" {
  description = "Reference to AWS profile that manages Route53"
  default     = "mkb"
}


#
# The following variables need to be configured in `env/<ENV>/terraform.tfvars`
#
# Where:
# `<ENV>` refers to the environment/stage/account you're deploying to.
#
# Format:
# key = "value"
#
# Example: service = "foobar"
#
variable "service" {
  description = "The name of the service"
}

variable "environment" {
  description = "The environment to which you want to deploy"
}

variable "stage" {
  description = "The stage that you want to deploy"
}

variable "repo_name" {
  description = "Name of this Git repository (passed as `TF_VAR_repo_name`)"
}
