# Terraform

## Getting Started

Install the following pre-requisite tools:

* [terraform](https://www.terraform.io/downloads.html)
* [terragrunt](https://github.com/gruntwork-io/terragrunt/releases)

## About Terragrunt
Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules.
You can use the `terragrunt` command instead of the `terraform` command to reach the same goals.

## Configure environment
> Make sure that you don't' use environment variables like `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` or `AWS_SESSION_TOKEN`
> If you do, ... the profile configured in the terraform code won't work properly!!

## Deployment
This project contains a [`Makefile`](https://www.gnu.org/software/make/manual/html_node/Introduction.html). Use it to simplify Terraform deployment:

### Prepare
*Per* stage, create a separate directory with a (custom) `terraform.tfvars` like this:
```
$ mkdir -p env/dev
$ cp env/default/terraform.tfvars.example env/dev/terraform.tfvars
```
*Note*: Update the variables in `env/<stage>/terraform.tfvars` before you continue.

### Develop

Add TF resources/modules to `main.tf`

### Deploy

```
$ make dev plan
...
$ make dev apply
```

The `Makefile` will take care of create a TF [workspace](https://www.terraform.io/docs/state/workspaces.html) if it's not already created. This is to ensure that the state, that belongs to this stage, is stored in a separate location in the configured S3 bucket.

# Hints & Tips

## About Terraform

Go read the documentation: https://www.terraform.io/docs/index.html

## Available Terraform Modules

* [terraform-aws-dynamodb](https://bitbucket.org/tomdevops/terraform-aws-dynamodb)
* [terraform-aws-acm-request-certificate](https://bitbucket.org/tomdevops/terraform-aws-acm-request-certificate)
* [terraform-aws-codebuild](https://bitbucket.org/tomdevops/terraform-aws-codebuild)

*Hint*: if you create a Terraform [module](https://www.terraform.io/docs/modules/create.html): evaluate with the team and create a separate BitBucket repository so it can be easily shared with others.

## Debugging all the things

```
$ TF_LOG=trace tf plan -var-file=env/default/terraform.tfvars
```

## Import existing resources

```
$ TF_LOG=trace tf import -var-file=env/default/terraform.tfvars  module.s3.aws_s3_bucket.default dev-alfred-upload
```

The trick is to use the name of the module as a reference.

```
module "s3" {
  source             = "modules/tf_aws_s3"
  region             = "${var.aws_region}"
  name               = "${var.env}-${var.service}-upload"
  versioning_enabled = "${var.s3_versioning}"
  force_destroy      = "${var.s3_force_destroy}"
}
```

Inside that module, you'll see (something like) the following:

```
resource "aws_s3_bucket" "default" {
  bucket        = "${var.name}"
  region        = "${var.region}"
  force_destroy = "${var.force_destroy}"
  acl           = "${var.acl}"
  ...
}
```

As a result, the 'path' to the resource is: `module.s3.aws_s3_bucket.default`

~ the end
