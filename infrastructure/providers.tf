#
# Updating provider version? Then run: `$ terraform init -upgrade`
#

# skipping: version = "~> 1.20.0"
provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

provider "aws" {
  alias   = "route53"
  profile = "${var.aws_route53_profile}"
  region  = "${var.aws_region}"
}

provider "template" {
  version = "1.0"
}

terraform {
  backend "s3" {}
}
